'use strict';

global.APPPATH = __dirname;
global.config = {};

let utils = require(`${APPPATH}/lib/utils.js`);

let Discord = require('discord.io');
global.client = new Discord.Client({
    token: process.env.DISCORD_OAUTH_TOKEN,
    autorun: true
});
global.isConnected = false;

let requireDir = require('require-dir');
let fs = require('fs');
let decache = require('decache');
let onFileChange = require('on-file-change');
let os = require('os');

let express = require('express');
let path = require('path');
let http = require('http');
let app = express();
let server = http.Server(app);

let pastemd = require('pastemd');
let hastebin = require(`${APPPATH}/lib/hastebin`);

let pastemdLocal = require('./lib/pastemd.js');

global.plugins = {};

let messageQueue = [];
let throttleTime = process.env.MESSAGE_THROTTLE_TIME * 1000;
let dequeueTimeout;
let lastSentAt;

global.say = (to, message) => {
    let now = Date.now();

    if (now < lastSentAt + throttleTime - 5) {
        messageQueue.push({
            to:  to,
            message: message
        });

        if (!dequeueTimeout) {
            dequeueTimeout = setTimeout(dequeue, now - lastSentAt + throttleTime);
        }

        return false;
    }

    client.sendMessage({ to: to, message: message });
    utils.trace(`Sent message: ${to} - ${message}`);

    lastSentAt = now;
    if (dequeueTimeout) {
        if (messageQueue.length) {
            dequeueTimeout = setTimeout(dequeue, throttleTime);
        } else {
            dequeueTimeout = null;
        }
    }

    return true;
}

let dequeue = () => {
    var item = messageQueue.shift();
    say(item.to, item.message);
}

// TODO: remove!
let resolveMessage = (to, message) => {
	/*if (message.indexOf('\n') >= 0) {
		let messages = message.split('\n');
        for (let r = 0; r < messages.length; r++) {
			say(to, messages[r]);
		}
	} else {*/
		say(to, message);
	//}
};

client.on('ready', () => {
    isConnected = true;
    utils.info(`Logged in as ${client.username} - (${client.id})`);

    // todo: check if changelog is valid and make a log if it's erroring
    let changelog = require('./changelog.json');
    let version = changelog.version;

    pastemdLocal.get('SmallGiganticCheetah', (error, data) => {
        if ((data.split(".").length - 1) !== 2) {
            return;
        }

        utils.info(`Current Version: ${version}`);
        utils.info(`Last Updated Version: ${data}`);
        if (version !== data) {
            let items = changelog.changelog[version];
            
            let output = '';    
            output += `Updated to **Version ${version}** \`\`\``;
            
            for (let i = 0; i < items.length; i++) {
                output += ` • ${items[i]}${os.EOL}`;
            }

            output += `\`\`\`${os.EOL}`;
            output += `Please use \`-help\` for information on commands.${os.EOL}`;
            output += `Please use \`-idea\` to make suggestions or bug reports.${os.EOL}`;
            output += `If you are interested in using NitxBot v${version} for your own server, please use \`-invite\`.${os.EOL}${os.EOL}`;
            output += `**Thanks for using NitxBot!**`

            let rooms = Object.keys(client.servers);
            for (let i = 0; i < rooms.length; i++) {
                client.sendMessage({ to: rooms[i], message: output });
            }
            
            pastemdLocal.edit('SmallGiganticCheetah', 'password', version, (error, data) => {

            });
        }
    });
});

client.on('disconnect', (erMsg, code) => {
    utils.info(`Bot disconnected from Discord with code '${code}' for reason: '${erMsg}'`);
    client.connect();
});

/*client.on('messageDelete', (message) => {
    let channelID = message.d['channel_id'];
    let messageID = message.d['id'];

    let deletedMessage = client._messageCache[channelID][messageID] || undefined;

    if (deletedMessage) {
        let content = deletedMessage.content;
        let userID = deletedMessage.author.id;

        if (userID === '223144453555224578') {
            return;
        }
        
        say(channelID, `The following message was deleted by either <@${userID}> or a user with the Manage Messages permission:`);
        say(channelID, `\`\`\`${content}\`\`\``);
    }
});*/

client.on('message', (user, userID, channelID, message, event) => {
    if (userID === client.id) {
        return;
    }
    
    let help = utils.commandify('help', message);
    if (help) {
        utils.trace(`Help requested by ${user}#${userID}`);
        /*if (helpURL) {
            say(channelID, `Bot commands: ${helpURL}`);
        } else {
            say(channelID, `The bot commands data is still populating, please try again in a moment.`);
            generateHelp();
        }*/
        say(channelID, helpText);
    }

    for (let p in plugins) {
        if (plugins[p].onchat) {
            plugins[p].onchat(user, userID, channelID, message, event, (callback) => {
                resolveMessage(callback.to, callback.message);
            });
        }

        if (plugins[p].commands) {
            for (let c = 0; c < plugins[p].commands.length; c++) {
                let args = utils.commandify(plugins[p].commands[c].aliases, message);
                if (args) {
                    utils.trace(`Command '${JSON.stringify(plugins[p].commands[c].aliases)}' used by ${user}#${userID} in ${channelID} with args: ${JSON.stringify(args)}`);
                    // if there's a whitelist, then check first.
                    if (plugins[p].commands[c].whitelist) {
                        if (plugins[p].commands[c].whitelist.indexOf(userID) == -1) {
                            say(channelID, `<@${userID}>, you are not whitelisted to use this command.`);
                            continue;
                        }
                    }

                    if (plugins[p].commands[c].requiredPermission) {
                        
                    }
                        
                    try {
                        plugins[p].commands[c].action(user, userID, channelID, event, args, (callback) => {
                            resolveMessage(callback.to, callback.message);
                        });
                    } catch (error) {

                        say(channelID, `An error occured in ${p}.js. Please report this with -complaint: ${error.name}: ${error.message}.`)
                        utils.error(JSON.stringify(error.stack));
                    }        
                }        
            }
        }
    }
});

let loadPlugin = (file, callback) => {
    try {
        let name = file.replace('.js', '');
        if (plugins[name]) {
            decache(APPPATH + `/plugins/${file}`);
        }

        plugins[name] = require(`./plugins/${file}`);

        if (plugins[name].startup) {
            plugins[name].startup(app);
        }

        if (callback) {
            callback(null);
        }
    } catch (error) {
        if (callback) {
            callback(error);
        }
    }
}

utils.info('Loading plugins...')
fs.readdirSync(APPPATH + '/plugins/').forEach(function (file) {
    if (file.match(/\.js$/) !== null) {
        utils.trace(`Loading ${file}...`);
        loadPlugin(file, (error) => {
            if (error) {
                utils.error(`Could not load ${file}:`);
                utils.error(`${error}.`);
            } else {
                utils.info(`Loaded ${file}.`);

                if (process.env.ENABLE_WATCHERS) {
                    onFileChange(`./plugins/${file}`, () => {
                        loadPlugin(file, (error) => {
                            if (error) {
                                utils.error(`Could not reload ${file}:`);
                                utils.error(`${error}`);
                            } else {
                                utils.info(`Reloaded ${file}`);
                            }
                        });
                        generateHelp();
                    });
                }
            }
        })
    }
})
utils.info('Finished loading plugins.');

// generate a hastebin of commands
let helpText;
let helpURL;
let generateHelp = () => {
    let help = '';

    for (let p in plugins) {
        if (plugins[p]) {
            help += `!== Commands provided by the ${p} plugin  ==!${os.EOL}`;
            let commands = plugins[p].commands;
            if (commands.length > 0) {
                for (let c = 0; c < commands.length; c++) {
                    let command = commands[c];
                    let first = (Array.isArray(command.aliases) ? command.aliases[0] : command.aliases);
                    
                    help += `\t${command.whitelist ? '[dev only] ' : ''}${process.env.COMMAND_CHAR}${first}`;
                    
                    if (command.usage) {
                        help += ` ${command.usage}`;
                    }
                    
                    if (command.description) {
                        help += `: ${command.description}`;
                    }
                    help += os.EOL;
                    
                    /*if (Array.isArray(command.aliases)) {
                        if (command.aliases.length > 1) {
                            help += ` (Aliases: ${command.aliases.slice(1).join(', ')})${os.EOL}`;
                        }
                    }*/
                }
            }
            help += os.EOL;
        }
    }

    help += `{} represent mandatory parameters. [] represent optional parameters.${os.EOL}`;
    help += `NitxBot is open source: https://gitlab.com/CarimA/NitxBotDiscord`;

    hastebin(help, (error, url) => {
        if (error) {
            utils.error(error);
        } else {
            helpURL = url;
            utils.info(`Help uploaded to '${helpURL}'.`);
        }
    });

    help = `\`\`\`diff${os.EOL}${help}\`\`\``;

    helpText = help;
}

generateHelp();

app.use('/', express.static(path.join(__dirname, 'public')));

app.get('/', (request, response, next) => {
    response.send('Nothing here!');
});

let port = process.env.PORT || 3000;
server.listen(port);