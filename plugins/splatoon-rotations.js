'use strict';

let request = require('request');
let moment = require('moment');
let utils = require(`${APPPATH}/lib/utils.js`);
let async = require('async')
let os = require('os');

let sayMaps = (channelID, sched, time, args, callback) => {
	request('https://splatoon.ink/schedule.json', (error, response, body) => {
		if (error) {
			callback('An error occured in reading the rotation data.');
		} else {
			let rotationData = JSON.parse(body);
			let remainingTime = 0;
			if (sched === 0) {
				remainingTime = moment(rotationData.schedule[sched].endTime).fromNow(); 
			} else {
				remainingTime = moment(rotationData.schedule[sched].startTime).fromNow();
			}
			
			let mode = rotationData.schedule[sched].ranked.rulesEN;
			let tw1 = rotationData.schedule[sched].regular.maps[0].nameEN;
			let tw2 = rotationData.schedule[sched].regular.maps[1].nameEN;
			let rn1 = rotationData.schedule[sched].ranked.maps[0].nameEN;
			let rn2 = rotationData.schedule[sched].ranked.maps[1].nameEN;

			if (args && (args[0] === 's' || args[0] === 'short')) {
				// wahahahaha this hacky solution
				callback({
					to: channelID,
					message:`${tw1.substr(0, tw1.indexOf(" "))}/${tw2.substr(0, tw2.indexOf(" "))} TW & ${rn1.substr(0, rn1.indexOf(" "))}/${rn2.substr(0, rn2.indexOf(" "))} ${mode.match(/\b(\w)/g).join('').replace('R', 'RM')}${sched === 0 ? '' : ' ' + remainingTime}`
				});
			} else {
				callback({
					to: channelID,
					message:`The ${time} **Turf War** maps are **${tw1}** & **${tw2}**.\nThe ${time} **${mode}** maps are **${rn1}** & **${rn2}**.\nMaps will change **${remainingTime}**.`
				});
			}
		}
	});
};

let votes = {};

/* structure 
	"roomID": {
		"status": 'open',
		"votes": {

		},
		"ips": {
			"192.168.0.1": "1"
		}
	}

*/

module.exports = {
	startup: (app) => {
		/*app.get('/:room/:vote', (request, response, next) => {

		});*/
	},
    commands: [
        /*{
            aliases: [ 'mapmodevote' ],
            requiredAuth: 'user',
            whisperUsers: false,
            description: 'Starts up a map/mode vote.',
            action: (user, userID, channelID, event, args, callback) => {
				let maps = [ 'Urchin Underpass', 'Walleye Warehouse', 'Arowana Mall', 'Saltspray Rig',
                'Kelp Dome', 'Bluefin Depot', 'Flounder Heights', 'Moray Towers',
                'Camp Triggerfish', 'Port Mackerel', 'Hammerhead Bridge', 'Blackbelly Skatepark', 
                'Mahi-Mahi Resort', 'Ancho-V Games', 'Museum d\'Alfonsino', 'Piranha Pit' ];
                let modes = [ 'Turf War', 'Rainmaker', 'Tower Control', 'Splat Zones' ];

				if (!votes[channelID]) {
					votes[channelID] = {
						'status': 'closed',
						'voteCount': {},
						'ips': {}
					};
				}

				if (votes[channelID].status === 'open') {
					callback({
						to: channelID,
						message: 'Voting is already open. Please wait for this vote to finish before starting another.'
					});
					return;
				}
				
                if (args) {
                    let index = -1;

                    if (args.indexOf('!tw') >= 0) {
                        index = modes.indexOf('Turf War');
                        modes.splice(index, 1);
                    }

                    if (args.indexOf('!sz') >= 0) {
                        index = modes.indexOf('Splat Zones');
                        modes.splice(index, 1);
                    }

                    if (args.indexOf('!tc') >= 0) {
                        index = modes.indexOf('Tower Control');
                        modes.splice(index, 1);
                    }

                    if (args.indexOf('!rm') >= 0) {
                        index = modes.indexOf('Rainmaker');
                        modes.splice(index, 1);
                    }

                    utils.trace(modes);

                    if (modes.length == 0) {
                        return 'Cannot remove all modes.';
                    }
                }

				callback({
					to: channelID,
					message: 'What map would you like to play? Vote by clicking the link'
				})
				sayMaps(channelID, 0, 'current', args, (maps) => callback(maps));
            }
        },*/
        {
            aliases: [ 'currentmaps', 'maps', 'currentrotation', 'rotation', 'schedule', 'currentschedule', 's' ],
            requiredAuth: 'user',
            whisperUsers: false,
            description: 'Posts the current Splatoon rotation.',
            action: (user, userID, channelID, event, args, callback) => {
				sayMaps(channelID, 0, 'current', args, (maps) => callback(maps));
            }
        },
        {
            aliases: [ 'nextmaps', 'nextrotation', 'nextschedule', 'ns'  ],
            requiredAuth: 'user',
            whisperUsers: false,
            description: 'Posts the next Splatoon rotation.',
            action: (user, userID, channelID, event, args, callback) => {				
				sayMaps(channelID, 1, 'next', args, (maps) => callback(maps));
            }
        },
        {
            aliases: [ 'latermaps', 'laterrotation', 'laterschedule', 'ls' ],
            requiredAuth: 'user',
            whisperUsers: false,
            description: 'Posts the later Splatoon rotation.',
            action: (user, userID, channelID, event, args, callback) => {
				sayMaps(channelID, 2, 'later', args, (maps) => callback(maps));
            }
        },
        {
            aliases: [ 'allmaps', 'allrotations' ],
            requiredAuth: 'user',
            whisperUsers: false,
            description: 'Posts the entire Splatoon rotation.',
            action: (user, userID, channelID, event, args, callback) => {
				let curMaps = '';
				let nextMaps = '';

				// ugly af solution.
				sayMaps(channelID, 0, 'current', args, (maps) => {
					curMaps = maps;

					sayMaps(channelID, 1, 'next', args, (maps) => {
						nextMaps = maps;

						sayMaps(channelID, 2, 'later', args, (maps) => {
							callback(curMaps);
							callback(nextMaps);
							callback(maps);
						});
					});
				});
            }
        }
    ]
};

