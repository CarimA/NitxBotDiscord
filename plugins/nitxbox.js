'use strict';

let utils = require(`${APPPATH}/lib/utils.js`);

module.exports = {
    startup: (app) => {

    },

    // server ID: 234787549401448448

    // #hello: 234787600160915457
    // #general: 234787549401448448
    // #announcements: 234787655840169985
    // #bot: 234789844583317505

    // @everyone: 234787549401448448
    // @member: 234788130979446784
    // @admin: 234788164189945866

    onchat: (user, userID, channelID, message, event, callback) => {
        if (channelID === '234787600160915457') {
            // it's #hello
            if (message === 'agree') {
                client.addToRole({
                    serverID: '234787549401448448',
                    userID: userID,
                    roleID: '234788130979446784'
                });

                callback({
                    to: '234787655840169985',
                    message: `**Please welcome <@${userID}> to NitxBox!** Say hi over on #general!` 
                });
            }

            // regardless of message, delete it.
            client.deleteMessage({
                channelID: channelID,
                messageID: event.d.id
            });
        }

        if (channelID === '234787549401448448') {
            // it's #general

        }
    },

    commands: [
    ]
};