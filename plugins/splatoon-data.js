'use strict';

//let dbb = require('dbb');
//let db = dbb('./plugins/splatoon-data/data.json');
let lev = require("levenshtein");
let pastemd = require('pastemd');
let os = require('os');
let utils = require(`${APPPATH}/lib/utils.js`);

let weapons = require('./splatoon-data/weapons.json');
let weaponClasses = require('./splatoon-data/weapon-classifications.json');

let subs = require('./splatoon-data/subs.json');
let specials = require('./splatoon-data/specials.json');

let filter = (action, isNegating, typical, not) => {
	let results = weapons.filter(action);
	if (isNegating) {
		not.push(results);
	} else {
		typical.push(results);
	}
}

Array.prototype.unique = function() {
    var a = this.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
};

let intersection = (arrayOfArrays) => {
	return arrayOfArrays.shift().reduce(function(res, v) {
		if (res.indexOf(v) === -1 && arrayOfArrays.every(function(a) {
			return a.indexOf(v) !== -1;
		})) 
		res = res.concat(v);
		return res;
	}, []);
};

let weaponSearch = (queries, callback) => {
	let notQueue = [];
	let orQueue = [];
	let andQueue = [];

	for (let i = 0; i < queries.length; i++) {
		let query = queries[i].toLowerCase().trim();
		query = query.replace(/\s/g, '');
		let negating = false;

		let only = false;

		if (query.charAt(0) === '!') {
			negating = true;
			query = query.substr(1, query.length);
		}

		if (query.match('^only') || query.match('only$')) {
			only = true;
			query = query.replace('only', '');
		}

		// check query concerning sub weapon
		if (query === 'splatbomb' || query === 'splatbombs') {
			filter((item) => { return item.sub === 'splatbomb'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}
		
		if (query === 'burstbomb' || query === 'burst' || query === 'burstbombs') {
			filter((item) => { return item.sub === 'burstbomb'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}
		
		if (query === 'disruptor' || query === 'disruptors') {
			filter((item) => { return item.sub === 'disruptor'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}

		if (query === 'inkmine' || query === 'inkmines' || query === 'mine' || query === 'mines') {
			filter((item) => { return item.sub === 'inkmine'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}

		if (query === 'pointsensor' || query === 'pointsensors') {
			filter((item) => { return item.sub === 'pointsensor'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}

		if (query === 'seeker' || query === 'seekers') {
			filter((item) => { return item.sub === 'seeker'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}

		if (query === 'splashwall' || query === 'splashwalls' || query === 'wall' || query === 'walls') {
			filter((item) => { return item.sub === 'splashwall'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}

		if (query === 'sprinkler' || query === 'sprinklers') {
			filter((item) => { return item.sub === 'sprinkler'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}
		
		if (query === 'squidbeakon' || query === 'squidbeakons' || query === 'beakon' || query === ' beakons' || query === ' beacon' || query === 'beacons') {
			filter((item) => { return item.sub === 'squidbeakon'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}

		if (query === 'suctionbomb' || query === 'suctionbombs' || query === 'suction') {
			filter((item) => { return item.sub === 'suctionbomb'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}

		// check query concerning special
		if (query === 'bombrush') {
			filter((item) => { return item.special === 'bombrush'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}
		
		if (query === 'bubbler' || query === 'bubble') {
			filter((item) => { return item.special === 'bubbler'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}
		
		if (query === 'echolocator' || query === 'echo') {
			filter((item) => { return item.special === 'echolocator'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}

		if (query === 'inkstrike') {
			filter((item) => { return item.special === 'inkstrike'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}
		
		if (query === 'inkzooka' || query === 'zooka' || query === 'bullcrap') {
			filter((item) => { return item.special === 'inkzooka'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}
		
		if (query === 'killerwail' || query === 'wail') {
			filter((item) => { return item.special === 'killerwail'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}
		
		if (query === 'kraken') {
			filter((item) => { return item.special === 'kraken'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}

		// check query concerning depletion
		if (query === 'lightdepletion') {
			filter((item) => { return item.depletion === 'light'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}

		if (query === 'mediumdepletion') {
			filter((item) => { return item.depletion === 'medium'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}

		if (query === 'heavydepletion') {
			filter((item) => { return item.depletion === 'heavy'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}

		// check query concerning class
		if (query === 'blaster' || query === 'blasters') {
			filter((item) => { return item.class === 'blaster'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}
		if (query === 'brush' || query === 'brushes') {
			filter((item) => { return item.class === 'brush'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}
		if (query === 'charger' || query === 'chargers' || query === 'sniper' || query === 'snipers') {
			filter((item) => { return item.class === 'charger'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}
		if (query === 'roller' || query === 'rollers') {
			filter((item) => { return item.class === 'roller'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}
		if (query === 'shooter' || query === 'shooters') {
			filter((item) => { return item.class === 'shooter'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}
		if (query === 'slosher' || query === 'sloshers' || query === 'bucket' || query === 'buckets') {
			filter((item) => { return item.class === 'slosher'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}
		if (query === 'splatling' || query === 'splatlings') {
			filter((item) => { return item.class === 'splatling'; }, negating, only ? andQueue : orQueue, notQueue); continue;
		}

		// AND QUERIES
		let inequalityCheck = (check, query, operator, leftAssoc, rightAssoc) => {
			let items = query.split(operator);
			if (items[0] === check) {
				let num = parseFloat(items[1]);
				filter((item) => { return leftAssoc(item, num) }, negating, andQueue, notQueue);
			} else {
				let num = parseFloat(items[0]);
				filter((item) => { return rightAssoc(item, num) }, negating, andQueue, notQueue);
			}
		};

		let comparisonTest = (testing, query, property) => {
			if (query.includes('>=')) {
				inequalityCheck(testing, query, '>=', 
				(item, num) => {
					return item[property] >= num;
				},
				(item, num) => {
					return num >= item[property];
				});				
			} else if (query.includes('<=')) {
				inequalityCheck(testing, query, '<=', 
				(item, num) => {
					return item[property] <= num;
				},
				(item, num) => {
					return num <= item[property];
				});				
			} else if (query.includes('=')) {
				inequalityCheck(testing, query, '=', 
				(item, num) => {
					return item[property] === num;
				},
				(item, num) => {
					return num === item[property];
				});
			} else if (query.includes('>')) {
				inequalityCheck(testing, query, '>', 
				(item, num) => {
					return item[property] > num;
				},
				(item, num) => {
					return num > item[property];
				});
			} else if (query.includes('<')) {
				inequalityCheck(testing, query, '<', 
				(item, num) => {
					return item[property] < num;
				},
				(item, num) => {
					return num < item[property];
				});			
			} else {
				return `Cannot use query '${testing}' without an equality operator.`;
			}
		};

		// check query concerning level
		if (query.includes('level')) {
			let result = comparisonTest('level', query, 'level');
			if (result) {
				callback(result, null);
				return;
			}
			continue;
		}

		// check query concerning cost
		if (query.includes('cost')) {
			let result = comparisonTest('cost', query, 'cost');
			if (result) {
				callback(result, null);
				return;
			}
			continue;
		}


		// check query concerning damage
		if (query.includes('damage')) {
			let result = comparisonTest('damage', query, 'damage');
			if (result) {
				callback(result, null);
				return;
			}
			continue;
		}

		// check query concerning range
		if (query.includes('range')) {
			let result = comparisonTest('range', query, 'range');
			if (result) {
				callback(result, null);
				return;
			}
			continue;
		}

		// check query concerning firerate
		if (query.includes('firerate')) {
			let result = comparisonTest('firerate', query, 'firerate');
			if (result) {
				callback(result, null);
				return;
			}
			continue;
		}

		// check query concerning ttkFrames
		if (query.includes('ttk')) {
			let result = comparisonTest('ttk', query, 'ttkFrames');
			if (result) {
				callback(result, null);
				return;
			}
			continue;
		}

		// check query concerning inkusage
		if (query.includes('inkusage')) {
			let result = comparisonTest('inkusage', query, 'inkusage');
			if (result) {
				callback(result, null);
				return;
			}
			continue;
		}

		// check query concerning mobility -- maybe skip until figure out
		// check query concerning minimumToKO
		if (query.includes('hitstokill')) {
			let result = comparisonTest('hitstokill', query, 'minimumToKO');
			if (result) {
				callback(result, null);
				return;
			}
			continue;
		}



		if (query === 'all') {
			continue;
		}

		// if we've reached the end, this query must be invalid.
		callback(`Parameter '${query}' is invalid.`, null);
		return;
	}

	// ok, now that we're here, find the interesection of everything in and.
	let andIntersection = [];
	if (andQueue.length > 1) {
		andIntersection = intersection(andQueue);
	} else if (andQueue.length === 1) {
		andIntersection = andIntersection.concat(andQueue[0]);
	}
	let output = [];

	if (andIntersection.length === 0) {
		if (orQueue.length === 0) {
			output = output.concat(weapons);
		} else {
			// if the intersection is empty, just add everything in or.
			for (let i = 0; i < orQueue.length; i++) {
				output = output.concat(orQueue[i]);
			}
		}
	} else {
		// if the intersection isn't empty, do intersect for each item in or and intersection and add
		if (orQueue.length > 0) {
			for (let i = 0; i < orQueue.length; i++) {
				output = output.concat(intersection([ orQueue[i], andIntersection ]));
			}
		} else {
			output = output.concat(andIntersection);
		}
	}

	// check if there's actually any found results
	if (output.length === 0) {
		if (notQueue.length === 0) {
			output = [].concat(weapons);
		} else {
			callback(`No results found. Did you perhaps use conflicting queries?`, null);
			return;
		}
	}

	// remove duplicates from the added final queue
	output = output.unique();

	notQueue = [].concat.apply([], notQueue);
	console.log(notQueue.length);
	
	if (notQueue.length > 0) {
		// remove duplicates from the not queue	
		notQueue = notQueue.unique();
	
		// remove whatever's in the not queue from the final queue
		output = output.filter((item) => {
			for (let i = 0; i < notQueue.length; i++) {
				if (notQueue[i].id === item.id) {
					return false;
				}
			}
			return true;
		});
	}

	// return the final queue
	callback(null, output);
}

module.exports = {
    commands: [
		{
            aliases: [ 'weaponsearch', 'ws' ],
            description: 'Searches for weapons based on provided queries. Quantitative data require inequalities.',
            usage: '[ sub-weapon ], [ special ], [ light depletion | medium depletion | heavy depletion ], [ shooter | blaster | brush | roller | charger | splatling | slosher ], [ range ]',
			action: (user, userID, channelID, event, args, callback) => {
				weaponSearch(args, (error, data) => {
					if (error) {
						callback({
							to: channelID,
							message: error
						});
					} else {
						let items = [];
						if (data.length > 10) {
							console.log(args);
							if (args.indexOf('all') > -1) {
								data.forEach((item) => {
									items.push(item.nameEN);
								});

								callback({
									to: channelID,
									message:`Found ${data.length} items: ${items.join(', ')}`
								});
							} else {
								for (let i = 0; i < 10; i++) {
									items.push(data[i].nameEN);
								}
								items.push(`and more! Add 'all' to your search to get them. *(Be warned, this might create some unappreciated spam)*`);
								callback({
									to: channelID,
									message: `Found ${data.length} items: ${items.join(', ')}`
								});
							}
						} else if (data.length > 0) {
							data.forEach((item) => {
								items.push(item.nameEN);
							});
							callback({
								to: channelID,
								message: `Found ${data.length} items: ${items.join(', ')}`
							});
						} else {
							callback({
								to: channelID,
								message: 'No items found'
							});
						}
					}
				})
			}
		},
		{
            aliases: [ 'randomweapon', 'rw' ],
            description: 'Returns a random weapon. Can include queries to limit scope.',
			usage: '[ sub-weapon ], [ special ], [ light depletion | medium depletion | heavy depletion ], [ shooter | blaster | brush | roller | charger | splatling | slosher ], [ range ]',
            action: (user, userID, channelID, event, args, callback) => {
				let items = [];
				
				// £ = user, $ = weapon
				let phrasing = [
					`£, perhaps you'd enjoy the **$**?`,
					`Give the **$** a try, £.`,
					`I picked the **$** for you, £!`,
					`The **$** sounds fun, £.`,
					`Hm...\nThe **$**, maybe, £?`,
					`£, you're probably good with the **$**!`,
					`£, use the **$**.`,
					`The **$** might make you salty. Use it anyway, £.`,
					`Looks like the **$**, £.`
				];
				
				let getPhrase = (weapon) => {
					let phrase = phrasing[Math.floor(Math.random() * phrasing.length)];
					phrase = phrase.replace('£', `<@${userID}>`);
					phrase = phrase.replace('$', weapon);
					return phrase;
				};
				
				weaponSearch(args, (error, data) => {
					
					let getRandomItem = () => {
						return data[Math.floor(Math.random() * data.length)].nameEN;
					}
					
					if (error) {
						callback({
							to: channelID,
							message: error
						});
					} else {
						callback({
							to: channelID,
							message:`${getPhrase(getRandomItem())}`
						});
					}
				});
            }
        },
		{
			aliases: [ 'weaponinfo', 'wi' ],
            description: 'Returns information about the specified weapon.',
			usage: '{ weapon }',
            action: (user, userID, channelID, event, args, callback) => {
				if (!args || !args[0]) {
					callback({
						to: channelID,
						message: `<@${userID}>: No weapon specified.`
					});
					return;
				}

				let weapon = args[0].toLowerCase().trim();
				weapon = weapon.replace(/\s/g, '');
				weapon = weapon.replace(/\./g, '');
				weapon = weapon.replace(/\-/g, '');
				weapon = weapon.replace(/\'/g, '');

				if (weapon === 'ttek') {
					weapon = weapon.replace('ttek', 'tentateksplattershot');
				}
				if (weapon === 'spro') {
					weapon = weapon.replace('spro', 'splattershotpro');
				}
				if (weapon === 'berry') {
					weapon = weapon.replace('berry', 'berrysplattershotpro');
				}
				if (weapon === 'berrypro') {
					weapon = weapon.replace('berrypro', 'berrysplattershotpro');
				}
				if (weapon === 'cherry') {
					weapon = weapon.replace('cherry', 'cherryh3nozzlenose');
				}
				if (weapon === 'jet') {
					weapon = weapon.replace('jet', 'jetsquelcher');
				}
				if (weapon === 'forge') {
					weapon = weapon.replace('forge', 'forgesplattershotpro');
				}
				if (weapon === 'forgepro') {
					weapon = weapon.replace('forgepro', 'forgesplattershotpro');
				}
				if (weapon === 'splash') {
					weapon = weapon.replace('splash', 'splashomatic');
				}
				if (weapon === 'neosplash') {
					weapon = weapon.replace('neosplash', 'neosplashomatic');
				}
				if (weapon === 'sploosh') {
					weapon = weapon.replace('sploosh', 'splooshomatic');
				}
				if (weapon === 'neosploosh') {
					weapon = weapon.replace('neosploosh', 'neosplooshomatic');
				}
				if (weapon === 'tentatek') {
					weapon = weapon.replace('tentatek', 'tentateksplattershot');
				}
				if (weapon === 'octoshot') {
					weapon = weapon.replace('octoshot', 'octoshotreplica');
				}
				if (weapon === 'neosploosh') {
					weapon = weapon.replace('neosploosh', 'neoneosplooshomatic');
				}
				if (weapon === 'wasabi') {
					weapon = weapon.replace('wasabi', 'wasabisplattershot');
				}
				if (weapon === 'crb') {
					weapon = weapon.replace('crb', 'customrangeblaster');
				}
				if (weapon === 'grim') {
					weapon = weapon.replace('grim', 'grimrangeblaster');
				}
				if (weapon === 'luna') {
					weapon = weapon.replace('luna', 'lunablaster');
				}
				if (weapon === 'lunaneo') {
					weapon = weapon.replace('lunaneo', 'lunablasterneo');
				}
				if (weapon === 'bamboo') {
					weapon = weapon.replace('bamboo', 'bamboozler14mki');
				}
				if (weapon === 'bamboo1') {
					weapon = weapon.replace('bamboo1', 'bamboozler14mki');
				}
				if (weapon === 'bamboo2') {
					weapon = weapon.replace('bamboo2', 'bamboozler14mkii');
				}
				if (weapon === 'bamboo3') {
					weapon = weapon.replace('bamboo3', 'bamboozler14mkiii');
				}
				if (weapon === 'carbon') {
					weapon = weapon.replace('carbon', 'carbonroller');
				}
				if (weapon === 'bullshit') {
					weapon = weapon.replace('bullshit', 'carbonroller');
				}
				if (weapon === 'corocoro') {
					weapon = weapon.replace('corocoro', 'corocorosplatroller');
				}
				if (weapon === 'krakonroller') {
					weapon = weapon.replace('krakonroller', 'krakonsplatroller');
				}
				if (weapon === 'dynamo') {
					weapon = weapon.replace('dynamo', 'dynamoroller');
				}
				if (weapon === 'hydra') {
					weapon = weapon.replace('hydra', 'hydrasplatling');
				}
				if (weapon === 'customhydra') {
					weapon = weapon.replace('customhydra', 'customhydrasplatling');
				}
				if (weapon === 'heavy') {
					weapon = weapon.replace('heavy', 'heavysplatling');
				}
				if (weapon === 'heavydeco') {
					weapon = weapon.replace('heavydeco', 'heavysplatlingdeco');
				}
				if (weapon === 'heavyremix') {
					weapon = weapon.replace('heavyremix', 'heavysplatlingremix');
				}
				if (weapon === 'remix') {
					weapon = weapon.replace('remix', 'heavysplatlingremix');
				}
				if (weapon === 'mini') {
					weapon = weapon.replace('mini', 'minisplatling');
				}
				if (weapon === 'zini') {
					weapon = weapon.replace('zini', 'zinkminisplatling');
				}
				if (weapon === 'remi') {
					weapon = weapon.replace('remi', 'refurbishedminisplatling');
				}
				if (weapon === 'refurb') {
					weapon = weapon.replace('refurb', 'refurbishedminisplatling');
				}
				if (weapon === 'grb') {
					weapon = weapon.replace('grb', 'grimrangeblaster');
				}
				if (weapon === 'grimrange') {
					weapon = weapon.replace('grimrange', 'grimrangeblaster');
				}
				if (weapon === 'customrange') {
					weapon = weapon.replace('customrange', 'customrangeblaster');
				}
				if (weapon === 'dual') {
					weapon = weapon.replace('dual', 'dualsquelcher');
				}
				if (weapon === 'customdual') {
					weapon = weapon.replace('customdual', 'customdualsquelcher');
				}
				if (weapon === 'e3k') {
					weapon = weapon.replace('e3k', 'eliter3k');
				}
				if (weapon === 'custome3k') {
					weapon = weapon.replace('custome3k', 'customeliter3k');
				}
				if (weapon === 'tri') {
					weapon = weapon.replace('tri', 'trislosher');
				}
				if (weapon === 'trinouveau') {
					weapon = weapon.replace('trinouveau', 'trisloshernouveau');
				}

				let allweapons = weapons.slice(0);

				allweapons.sort((a, b) => {
					let aLev = new lev(a.id, weapon).distance;
					let bLev = new lev(b.id, weapon).distance;
					//console.log(`${weapon} against ${a.id} = ${aLev}`);
					//console.log(`${weapon} against ${b.id} = ${bLev}`);
					if (aLev > bLev) {
						return 1;
					} else if (aLev < bLev) {
						return -1;
					} else {
						return 0;
					}
				});

				let found = allweapons[0];
				
				let foundDist = new lev(found.id, weapon).distance;

				if (foundDist >= 8) {
					callback({
						to: channelID,
						message: `'${args[0].trim()}' not found. Did you spell it correctly?`
					});
					return;
				} else if (foundDist >= 5 && foundDist < 8) {
					callback({
						to: channelID,
						message: `'${args[0].trim()}' not found. Did you possibly mean any of the following: '${allweapons[0].nameEN}', '${allweapons[1].nameEN}', '${allweapons[2].nameEN}', '${allweapons[3].nameEN}', '${allweapons[4].nameEN}'?'`
					});
					return;
				} else if (foundDist > 0) {
					callback({
						to: channelID,
						message: `Could not find '${args[0].trim()}', using '${found.nameEN}' instead.`
					});
				}

				let subName = subs[found.sub].nameEN;
				let specialName = specials[found.special].nameEN;

				let output = '';

				output += (`**${found.nameEN.toUpperCase()}** *(${found.class} class)*`) + os.EOL;
				output += (`Sub Weapon: **${subName}**, Special: **${specialName}**`) + os.EOL;
				output += (`Unlocked at **Level ${found.unlocked}**. Costs **${found.cost}c**.`) + os.EOL;

				if (found.depletion === 'light') {
					output += (`Special Depletion: **Light** (40% Loss)`) + os.EOL;
				} else if (found.depletion === 'medium') {
					output += (`Special Depletion: **Medium** (60% Loss)`) + os.EOL;
				} else {
					output += (`Special Depletion: **Heavy** (75% Loss)`) + os.EOL;
				}

				let damage = found.damage;
				
				let max = (0.99 * 57 - Math.pow((0.09 * 57), 2)) / 100;
				let min = (0.99 * 0 - Math.pow((0.09 * 0), 2)) / 100;

				let minDamage = damage * (1 + (min - max) / 1.8);
				let maxDamage = damage * (1 + (max - min));

				switch (found.minimumToKO) {
					case 2: 
						maxDamage = Math.min(99.9, maxDamage);
						break;
					case 3: 
						maxDamage = Math.min(49.9, maxDamage);
						break;
					case 4: 
						maxDamage = Math.min(33.3, maxDamage);
						break;
					case 5: 
						maxDamage = Math.min(24.9, maxDamage);
						break;
				}

				minDamage = minDamage.toFixed(1);
				maxDamage = maxDamage.toFixed(1);

				let damagePhrase = `**${damage}** (Ranges from ${minDamage} - ${maxDamage})`;

				switch(found.class) {
					case 'splatling': 
						output += (`Damage per bullet: ${damagePhrase}`) + os.EOL;
						output += (`Turfing range: **${found.range}**`) + os.EOL;
						output += (`Minimum bullets to KO: **${found.minimumToKO}**`) + os.EOL;
						output += (`Time to kill: **${found.ttkSeconds} seconds**/**${found.ttkFrames} frames**`) + os.EOL;
						output += (`Full charge fires every **${found.firerate} frames**.`) + os.EOL;
						output += (`Each full charge uses **${found.inkusage}%** of your tank.`);

						break;
					case 'charger':
						output += (`Damage when fully charged: ${damagePhrase}`) + os.EOL;
						output += (`Range: **${found.range}**`) + os.EOL;
						output += (`Time to kill: **${found.ttkSeconds} seconds**/**${found.ttkFrames} frames**`) + os.EOL;
						output += (`Full charge fires every **${found.firerate} frames**.`) + os.EOL;
						output += (`Each full charge uses **${found.inkusage}%** of your tank.`) + os.EOL;

						break;
					case 'blaster':
						output += (`Damage (Direct Hit): ${damagePhrase}`) + os.EOL;
						output += (`Range: **${found.range}**`) + os.EOL;
						output += (`Time to kill *[Assuming Direct Hit(s)]*: **${found.ttkSeconds} seconds**/**${found.ttkFrames} frames**`) + os.EOL;
						output += (`Fires every **${found.firerate} frames**.`) + os.EOL;
						output += (`Each pellet uses **${found.inkusage}%** of your tank.`) + os.EOL;

						break;

					case 'roller':
					case 'brush':
						output += (`Damage per flick: ${damagePhrase}`) + os.EOL;
						output += (`Turfing range: **${found.range}**`) + os.EOL;
						output += (`Time to kill: **${found.ttkSeconds} seconds**/**${found.ttkFrames} frames**`) + os.EOL;
						output += (`Flicks every **${found.firerate} frames**.`) + os.EOL;
						output += (`Each flick uses **${found.inkusage}%** of your tank.`) + os.EOL;

						break;

					case 'slosher':
						output += (`Damage per slosh: ${damagePhrase}`) + os.EOL;
						output += (`Range: **${found.range}**`) + os.EOL;
						output += (`Time to kill: **${found.ttkSeconds} seconds**/**${found.ttkFrames} frames**`) + os.EOL;
						output += (`Sloshes every **${found.firerate} frames**.`) + os.EOL;
						output += (`Each slosh uses **${found.inkusage}%** of your tank.`) + os.EOL;
						break;

					default:
						output += (`Damage per bullet: ${damagePhrase}`) + os.EOL;
						output += (`Turfing range: **${found.range}**`) + os.EOL;
						output += (`Minimum bullets to KO: **${found.minimumToKO}**`) + os.EOL;
						output += (`Time to kill: **${found.ttkSeconds} seconds**/**${found.ttkFrames} frames**`) + os.EOL;
						output += (`Fires every **${found.firerate} frames**.`) + os.EOL;
						output += (`Each bullet uses **${found.inkusage}%** of your tank.`) + os.EOL;
				}
				
				callback({
					to: channelID,
					message: output
				});
            }
		}
    ]
};