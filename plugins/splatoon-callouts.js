'use strict';

let lev = require("levenshtein");

module.exports = {
    commands: [
        {
            aliases: [ 'callout', 'callouts' ],
            description: 'Posts an image of callouts for maps. Sourced from Inkipedia.',
            usage: '{ map }',
            action: (user, userID, channelID, event, args, callback) => {
				if (!args || !args[0]) {
					callback({
						to: channelID,
						message: `<@${userID}>: No map specified.`
					});
					return;
				}

                let maps = [ 'urchinunderpass', 'kelpdome', 'arowanamall', 'saltsprayrig',
                'bluefindepot', 'blackbellyskatepark', 'portmackerel', 'anchovgames',
                'camptriggerfish', 'flounderheights', 'hammerheadbridge', 'mahimahiresort',
                'moraytowers', 'museumdalfonsino', 'piranhapit', 'walleyewarehouse' ];

                let map = args[0].toLowerCase().trim();
				map = map.replace(/\s/g, '');
				map = map.replace(/\-/g, '');
				map = map.replace(/\'/g, '');

                let sanityCheck = (input, result) => {
                    if (map === input) {
                        map = map.replace(input, result);
                    }
                };
                
                sanityCheck('uu', 'urchinunderpass');
                sanityCheck('kd', 'kelpdome');
                sanityCheck('am', 'arowanamall');
                sanityCheck('sr', 'saltsprayrig');
                sanityCheck('bd', 'bluefindepot');
                sanityCheck('bs', 'blackbellyskatepark');
                sanityCheck('pm', 'portmackerel');
                sanityCheck('ag', 'anchovgames');
                sanityCheck('ct', 'camptriggerfish');
                sanityCheck('fh', 'flounderheights');
                sanityCheck('hb', 'hammerheadbridge');
                sanityCheck('mmr', 'mahimahiresort');
                sanityCheck('mr', 'mahimahiresort');
                sanityCheck('mt', 'moraytowers');
                sanityCheck('ma', 'museumdalfonsino');
                sanityCheck('mda', 'museumdalfonsino');
                sanityCheck('pp', 'piranhapit');
                sanityCheck('ww', 'walleyewarehouse');

                sanityCheck('warehouse', 'walleyewarehouse');
                sanityCheck('walleye', 'walleyewarehouse');
                sanityCheck('saltpit', 'piranhapit');
                sanityCheck('pit', 'piranhapit');
                sanityCheck('piranha', 'piranhapit');
                sanityCheck('museum', 'museumdalfonsino');
                sanityCheck('moray', 'moraytowers');
                sanityCheck('towers', 'moraytowers');
                sanityCheck('mahi', 'mahimahiresort');
                sanityCheck('mahimahi', 'mahimahiresort');
                sanityCheck('resort', 'mahimahiresort');
                sanityCheck('anchov', 'anchovgames');
                sanityCheck('games', 'anchovgames');
                sanityCheck('camp', 'camptriggerfish');
                sanityCheck('trigger', 'camptriggerfish');
                sanityCheck('triggerfish', 'camptriggerfish');
                sanityCheck('flounder', 'flounderheights');
                sanityCheck('hammerhead', 'hammerheadbridge');
                sanityCheck('bridge', 'hammerheadbridge');
                sanityCheck('bullshit', 'hammerheadbridge');
                sanityCheck('urchin', 'urchinunderpass');
                sanityCheck('underpass', 'urchinunderpass');
                sanityCheck('kelp', 'kelpdome');
                sanityCheck('dome', 'kelpdome');
                sanityCheck('arowana', 'arowanamall');
                sanityCheck('mall', 'arowanamall');
                sanityCheck('salt', 'saltsprayrig');
                sanityCheck('saltspray', 'saltsprayrig');
                sanityCheck('rig', 'saltsprayrig');
                sanityCheck('bluefin', 'bluefindepot');
                sanityCheck('depot', 'bluefindepot');
                sanityCheck('blackbelly', 'blackbellyskatepark');
                sanityCheck('skatepark', 'blackbellyskatepark');
                sanityCheck('port', 'portmackerel');
                sanityCheck('mackerel', 'portmackerel');

                maps.sort((a, b) => {
                    let aLev = new lev(a, map).distance;
                    let bLev = new lev(b, map).distance;

					if (aLev > bLev) {
						return 1;
					} else if (aLev < bLev) {
						return -1;
					} else {
						return 0;
					}
                });

				let found = maps[0];				
				let foundDist = new lev(found, map).distance;

				if (foundDist >= 4) {
					callback({
						to: channelID,
						message: `'${args[0].trim()}' not found. Did you spell it correctly?`
					});
					return;
				} 
                
				callback({
				    to: channelID,
					message: `${process.env.SITE_URL}/splatoon-callouts/${found}.png`
				});
                
				callback({
				    to: channelID,
					message: `Source: Inkipedia ↣ Callouts`
				});
            }
        }
    ]
};