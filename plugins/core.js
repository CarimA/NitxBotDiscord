'use strict';

let fs = require('fs');
let os = require('os');
let pastemd = require('pastemd');
let hastebin = require(`${APPPATH}/lib/hastebin`);
let utils = require(`${APPPATH}/lib/utils.js`);

module.exports = {
    startup: (app) => {

    },

    onchat: (user, userID, channelID, message, event, callback) => {
        
    },

    commands: [
        {
            aliases: [ 'about' ],
            description: 'Posts a summary of information about the bot.',
            action: (user, userID, channelID, event, args, callback) => {
                callback({
                    to: channelID, 
                    message: `I am NitxBot 2 (Now Discord-flavoured!), a smarter bot made in NodeJS. I am plugin-based, allowing content creators full freedom over any aspect of my functionality.`
                });
            }
        },
        {
            aliases: [ 'git', 'source', 'sourcecode' ],
            description: 'Posts a link to the GitLab repository for the bot.',
            action: (user, userID, channelID, event, args, callback) => {
                callback({
                    to: channelID, 
                    message: `GitLab repository: **https://gitlab.com/CarimA/NitxBotDiscord**`
                });
            }
        },
        {
            aliases: [ 'idea', 'bugreport', 'suggestion', 'suggest', 'complaint' ],
            description: 'Makes a report to the developer.',
            usage: '{ message }',
            action: (user, userID, channelID, event, args, callback) => {
                if (args && args.length >= 1) {
                    callback({
                        to: '243527173200281601',
                        message: `From ${user}#${userID}: ${args.join(', ')}`
                    });
                    callback({
                        to: channelID, 
                        message: 'Thank you for your idea/bug report. Please do continue to suggest ideas or report problems.'
                    });
                } else {
                    callback({
                        to: channelID,
                        message: 'Usage: -idea {message}'
                    });
                }
            }
        },
        {
            aliases: [ 'invite' ],
            description: 'Provides a link that allows you to invite NitxBot to your own server.',
            action: (user, userID, channelID, event, args, callback) => {
                callback({
                    to: channelID,
                    message: `Want me to come to your own server? Head on to https://discordapp.com/oauth2/authorize?client_id=223144453555224578&scope=bot and select your server under the dropdown menu and hit *Authorize*. Note that you require **Manage Server** permissions in order to invite me somewhere!`
                });
            }
        },
        {
            aliases: [ 'pin', 'pass', 'password', 'passcode', 'code' ],
            description: 'Generates a PIN between 0000 and 9999.', // If arguments are provided only the users provided will be messaged the PIN.',
            //usage: '-pin «comma-separated users (eg. @NitxBot, @Carim)»',
            action: (user, userID, channelID, event, args, callback) => {
                let min = 0;
                let max = 9999;
                let pin = utils.blockify(("0" + (Math.floor(Math.random() * (max - min + 1)) + min)).substr(-4));

                if (event.d.mentions.length > 0) {
                    // only message these people
                    for (let i = 0; i < event.d.mentions.length; i++) {
                        callback({
                            to: event.d.mentions[i].id,
                            message: `Your randomly generated **PIN** is ${pin}` 
                        });
                    }
                    callback({
                        to: userID,
                        message: `Your randomly generated **PIN** is ${pin}` 
                    });
                } else {
                    callback({
                        to: channelID,
                        message: `Your randomly generated **PIN** is ${pin}` 
                    });
                }

                //if (args && args.length > 0) {

                //} else {
                //}
            } 
        },
        {
            aliases: [ 'eval', 'evaluate', 'js' ],
            whitelist: [ '104711168601415680' ],
            description: 'Evaluates a JavaScript expression.',
            usage: '{ expression }',
            action: (user, userID, channelID, event, args, callback) => {
                try {
                    callback({
                        to: channelID,
                        message: JSON.stringify(eval(args.join(', ')))
                    });
                } catch(error) {
                    callback({
                        to: channelID,
                        message: `Error: [${error.name}: ${error.message}]`
                    });
                }
            }
        },
        {
            aliases: [ 'shutdown', 'kill' ],
            whitelist: [ '104711168601415680' ],
            description: 'Shuts down the bot.',
            action: (user, userID, channelID, event, args, callback) => {
                process.exit(-1);
            }
        },
        {
            aliases: [ 'say' ],
            whitelist: [ '104711168601415680' ],
            description: 'Allows a message to be sent through the bot.',
            usage: `{ channel ID }, { message }`,
            action: (user, userID, channelID, event, args, callback) => {
                if (!args || args.length <= 1) {
                    callback({
                        to: channelID,
                        message: 'Usage: -say {channel id}, {message}'
                    })
                    return;
                }

                callback({
                    to: args[0],
                    message: args.join(', ')
                });
            }
        }/*,
        {
            aliases: [ 'answer', 'tellme' ],
            description: 'Sends a query to Wolfram Alpha answering any question.',
            usage: `{ query }`,
            action: (user, userID, channelID, event, args, callback) => {
                wolfram.query(args.join(', '), (error, result) => {
                    if (error) {
                        callback({
                            to: channelID,
                            message: `<@${userID}>, an error occured. Unable to answer.`
                        });
                    } else {
                        if (result.length === 0) {
                            callback({
                                to: channelID,
                                message: `<@${userID}>, no results were found.`
                            });                          

                            return;
                        }

                        callback({
                            to: channelID,
                            message: `<@${userID}>, here is your answer`
                        });
                        
                        for (let i = 0; i < result.length; i++) {
                            if (result[i].primary) {
                                callback({
                                    to: channelID,
                                    message: `**${result[i].title}**: ${result[i].subpods[0].value}`
                                });
                            }
                        }
                    }
                });
            }
        }/*, 
        {
            aliases: [ 'logs' ],
            whitelist: [ '104711168601415680' ],
            description: 'Provides a link of bot logs.',
            action: (user, userID, channelID, event, args, callback) => {
                hastebin(utils.getLogs(), (error, url) => {
                    if (error) {
                        utils.error(error);
                    } else {
                        callback({
                            to: channelID,
                            message: `Logs uploaded to **${url}**`
                        });
                        utils.info('Uploaded logs to: ' + url);
                    }
                });
            }
        }*/
    ]
};