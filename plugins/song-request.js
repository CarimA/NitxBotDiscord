'use strict';

let lev = require("levenshtein");
let getYouTubeID = require('get-youtube-id');
let ytdl = require('ytdl-core');
let ffmpeg = require('fluent-ffmpeg');

let rooms = {};
let queue = {};

module.exports = {
    startup: (app) => {

    },

    onchat: (user, userID, channelID, message, event, callback) => {
        
    },

    commands: [
        {
            aliases: [ 'startmusic' ],
            description: 'Allows NitxBot to enter a Voice Channel to start playing music.',
            action: (user, userID, channelID, event, args, callback) => {
                let server = client.channels[channelID].guild_id;

                if (args && args[0]) {
                    // find the voice channel that matches up with the name given
                    for (let c in client.servers[server].channels) {
                        let channel = client.servers[server].channels[c];
                        if (channel.type === 'voice') {
                            let channelName = channel.name.toLowerCase().trim();
                            let inputName = args[0].toLowerCase().trim();
					        let length = new lev(channelName, inputName).distance;
                            
                            if (length < 4) {
                                if (rooms[server]) {
                                    client.leaveVoiceChannel(rooms[server]);
                                }
                                rooms[server] = c;

                                client.joinVoiceChannel(c);
                                callback({
                                    to: channelID, 
                                    message: `Joined ${channel.name}. Request songs with \`-requestsong {Youtube URL}\`. Stop me with \`-stopmusic\``
                                });
                                return;
                            }
                        }
                    }

                    // not found
                    callback({
                        to: channelID, 
                        message: `I couldn't find this voice channel. Is it spelled correctly, and do I have permission to join it?`
                    });
                } else {
                    // tell the user to provide the name of the voice channel
                    callback({
                        to: channelID, 
                        message: `Please provide the name of the voice channel you want me to join.`
                    });
                }

            }
        },
        {
            aliases: [ 'stopmusic' ],
            description: 'Makes NitxBot leave the current Voice Channel it is in.',
            action: (user, userID, channelID, event, args, callback) => {
                let server = client.channels[channelID].guild_id;

                if (rooms[server]) {
                    client.leaveVoiceChannel(rooms[server]);
                    rooms[server] = undefined;
                }
            }
        },
        {
            aliases: [ 'requestsong', 'rs' ],
            description: 'Queues and requests a song.',
            action: (user, userID, channelID, event, args, callback) => {
                let server = client.channels[channelID].guild_id;

                if (rooms[server]) {
                    if (args && args[0]) {
                        let id = getYouTubeID(args[0]);
                        if (id) {
                            let options = {
                                quality: 'highest',
                                downloadURL: true,
                                filter: 'audioonly'
                            };
                            let stream = ytdl(`https://www.youtube.com/watch?v=${id}`, options);
                            stream.on('info', (info, format) => {
                                let title = info.title; 
                                let author = info.author;
                                let length = info.length_seconds;

                                if (length > 60 * 10) {
                                    callback({
                                        to: channelID, 
                                        message: `Song length exceeds 10 minutes. Please find a shorter song to submit.`
                                    });
                                    return;
                                }

                                callback({
                                    to: channelID, 
                                    message: `${title} by ${author} is processing. This song will be added to the queue when audio processing is complete.`
                                });

                                // force deploy

                                let ff = new ffmpeg(stream)
                                    .audioBitrate(256)
                                    .saveToFile(`${id}.wav`)
                                    .on('end', () => {
                                        // add to queue.
                                        queue[server].push(id);

                                        client.getAudioContext(rooms[server], (s) => {
                                            s.playAudioFile(`${id}.wav`);
                                        });
                                    });
                            })
                            .on('error', (e) => {

                            });

                        } else {
                            callback({
                                to: channelID, 
                                message: `This is not a valid YouTube video. Do note that playlists cannot be used.`
                            });
                        }
                    } else {
                        callback({
                            to: channelID, 
                            message: `Please enter a YouTube URL or ID.`
                        });
                    }
                } else {
                    callback({
                        to: channelID, 
                        message: `I am not currently taking song requests in any voice channel. Please use -startmusic to have me start taking requests.`
                    });
                }
            }
        }
    ]
};