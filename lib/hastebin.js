'use strict';

let needle = require('needle');

module.exports = (data, callback) => {
    if (!data) {
        throw new Error('No data provided');
    }

    if (!callback) {
        throw new Error('No callback(err, data) provided');
    }

    postData(data, (error, response) => {
        if (error) {
            callback(error, null);
        } else {
            callback(null, response);
        }
    });            
};

let postData = (input, callback) => {
    needle.post('http://www.hastebin.com/documents', input, (error, response) => {
        if (error) {
            callback(error, null);
        } else {
            if (response.body.key) {
                let url = `http://www.hastebin.com/${response.body.key}`;
                callback(null, url);
            } else {
                callback(response.body.message, null);
            }
        }
    });
};