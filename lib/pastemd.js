'use strict';

let needle = require('needle');

module.exports = (data, arg1, arg2) => {
    if (!data) {
        throw new Error('No data provided');
    }
    
    if (arg1) {
        if (typeof arg1 === 'string' || arg1 instanceof String) {
            // arg1 is a string, therefore it's the password.
            let password = arg1;

            postData({ 
                'content': data,
                'password': password
            }, (error, response) => {
                if (error) {
                    arg2(error, null);
                } else {
                    arg2(null, response);
                }
            });
        } else {
            // arg1 is not a string, so it's the callback. arg2 is unused.
            postData({ 
                'content': data,
            }, (error, response) => {
                if (error) {
                    arg1(error, null);
                } else {
                    arg1(null, response);
                }
            });
        }
    } else {
        throw new Error("pastemd(data, <<password>>, callback); cannot be used without a callback.");
    }
};

module.exports.get = (id, callback) => {
    if (!id) {
        throw new Error('ID not provided');
    }

    needle.get(`http://pastemd.xyz/raw/${id}`, (error, response) => {
        if (error) {
            callback(error, null);
        } else {
            callback(null, response.body);
        }
    });
}

module.exports.edit = (id, password, content, callback) => {
    if (!id || !password || !content) {
        throw new Error('Input not complete: provide ID, password and content');
    }

    postEditData({
        'id': id,
        'content': content,
        'password': password
    }, (error, response) => {
        if (error) {
            callback(error, null);
        } else {
            callback(null, response);
        }
    });
};

let postData = (input, callback) => {
    needle.post('http://pastemd.xyz/api', input, (error, response) => {
        if (error) {
            callback(error, null);
        } else {
            if (response.body.longidDisplay) {
                let url = `http://pastemd.xyz/${response.body.longidDisplay}`;
                callback(null, url);
            } else {
                callback(response.body.error, null);
            }
        }
    });
};
let postEditData = (input, callback) => {
    needle.post('http://pastemd.xyz/api/edit/' + input.id + '/', input, (error, response) => {
        if (error) {
            callback(error, null);
        } else {
            if (response.body.longidDisplay) {
                let url = `http://pastemd.xyz/${input.id}`;
                callback(null, url);
            } else {
                callback(response.body.error, null);
            }
        }
    });
};