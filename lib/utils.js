'use strict';

let os = require('os');

module.exports = {    
    blockify: (input) => {
        input = input.replace(/0/g, ':zero: ');
        input = input.replace(/1/g, ':one: ');
        input = input.replace(/2/g, ':two: ');
        input = input.replace(/3/g, ':three: ');
        input = input.replace(/4/g, ':four: ');
        input = input.replace(/5/g, ':five: ');
        input = input.replace(/6/g, ':six: ');
        input = input.replace(/7/g, ':seven: ');
        input = input.replace(/8/g, ':eight: ');
        input = input.replace(/9/g, ':nine: ');
        return input;
    },
	commandify: (commands, message) => {
		// check that both data is valid
		if (!commands || !message) {
			return;
		}
		
		// check if message is actually a command...
		if (message.substr(0, process.env.COMMAND_CHAR.length) !== process.env.COMMAND_CHAR) {
			return null;
		}
		
		// check if it's a range of commands...
		if (commands instanceof Array) {
			for (let i = 0; i < commands.length; i++) {
				let output = module.exports.commandify(commands[i], message);
				if (output) {
					return output;
				}
			}
			return null;
		}
		
		// strip out the command character.
		let data = message.substr(process.env.COMMAND_CHAR.length);
		
		let command = data.split(' ')[0];
		
		// the command doesn't match. exit out.
		if (command.toLowerCase() !== commands.toLowerCase()) {
			return null;
		}

		// strip out the command.
		data = data.substr(command.length).trim();

		// split into arguments.
		let args = data.match(/(".*?"|[^",]+)(?=\s*,|\s*$)/g);		
		args = args || [];

        for (let i = 0; i < args.length; i++) {
            args[i] = args[i].trim();
        }
		
		return args;		
	},
	
    kill: (message) => {
        module.exports.fatal(message);
        process.exit(-1);
    },
    
	formatDate: (date) => {
	    var hours = date.getHours();
	    var mins  = date.getMinutes();

	    hours = (hours < 10 ? "0" : "") + hours;
	    mins = (mins < 10 ? "0" : "") + mins;

	    return `${hours}:${mins}`;
	},

    sanitise: (text) => {
        return text.toLowerCase().replace(/[^a-z0-9]/g, '');
    },

    setLogLevel: (level) => {
        currentLevel = level;
    },
    trace: log('trace'),
    debug: log('debug'),
    info: log('info'),
    warn: log('warn'),
    error: log('error'),
    fatal: log('fatal'),

    getLogs: () => {
        return out;
    }
}

let currentLevel = 'trace';
let levels =  {
    'trace': 0,
    'debug': 1,
    'info': 2,
    'warn': 3,
    'error': 4,
    'fatal': 5
};

let out = '';

function log(level) {
    return (message) => {
        let print = `[${module.exports.formatDate(new Date())}] ${level}: ${message}`;
        if (global.isConnected) {
            if (out !== '') {
                client.sendMessage({
                    to: '243527244142608384',
                    message: out
                });
                out = '';
            }

            client.sendMessage({
                to: '243527244142608384',
                message: print
            });
        } else {
            // buffer everything and send it when we're ready.
            out += print + os.EOL;
        }
        //out += print + os.EOL;

        // log it to console
        if (levels[level] >= levels[currentLevel]) {
            console.log(print);
        }
    }
}